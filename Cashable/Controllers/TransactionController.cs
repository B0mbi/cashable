﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cashable.Domain.DataModels;
using Cashable.Domain.Entities;
using Cashable.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cashable.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionRepository mTransaction;
        public TransactionController(ITransactionRepository transaction)
        {
            mTransaction = transaction;
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public IEnumerable<TransactionEntity> Get()
        {
            return mTransaction.Get();
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public TransactionEntity Get(int id)
        {
            return mTransaction.Get(id);
        }

        // POST api/<ValuesController>
        [HttpPost]
        public void Post([FromBody] TransactionEntity newTransaction)
        {
            mTransaction.Add(newTransaction);
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] UpdateTransactionDataModel updateTransaction)
        {
            var destTransaction = mTransaction.Get(id);
            destTransaction += updateTransaction;
            mTransaction.SaveChanges();
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            mTransaction.Delete(id);
        }
    }
}
