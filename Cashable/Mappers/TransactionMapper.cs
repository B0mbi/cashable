﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Cashable.Domain.DataModels;
using Cashable.Domain.Entities;

namespace Cashable.Mappers
{
    public class TransactionMapper
    {
        private IMapper mMapper;

        public TransactionMapper()
        {
            mMapper = new MapperConfiguration(config =>
            {
                config.CreateMap<NewTransactionDataModel, TransactionEntity>();
                config.CreateMap<UpdateTransactionDataModel, TransactionEntity>();
            }).CreateMapper();

    }

        public TransactionEntity Map<T>(T transactionDataModel) //where T
        {
            return mMapper.Map<TransactionEntity>(transactionDataModel);
        }
    }
}
