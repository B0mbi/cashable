﻿using System.Collections.Generic;
using System.Linq;
using Cashable.Domain.Entities;
using Cashable.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Cashable.DataBase.Repositories
{
    public class TransactionRepository : BaseRepository<TransactionEntity>, ITransactionRepository
    {
        protected override DbSet<TransactionEntity> DbSet => mDbContext.Transactions;

        public TransactionRepository(CashableDbContext dbContext) :  base(dbContext) { }

        public TransactionEntity Get(int id)
        {
            var data = DbSet.Where(x => x.ID == id).FirstOrDefault();
            return data;
        }
        public bool Add(TransactionEntity transactionEntity)
        {
            DbSet.Add(transactionEntity);
            if (mDbContext.SaveChanges() > 0)
                return true;
            return false;
        }

        public bool Update(int id, TransactionEntity transactionEntity)
        {
            var transaction = Get(id);
            if (transaction != null)
            {
                transaction.Name = transactionEntity.Name;
                transaction.Value = transactionEntity.Value;
                transaction.Date = transactionEntity.Date;
                if (mDbContext.SaveChanges() > 0)
                    return true;
            }
            return false;
        }

        public bool Delete(int id)
        {
            var transaction = Get(id);
            if (transaction != null)
            {
                DbSet.Remove(transaction);
                if (mDbContext.SaveChanges() > 0)
                    return true;
            }
            return false;
        }

        public void SaveChanges()
        {
            mDbContext.SaveChanges();
        }
    }
}
