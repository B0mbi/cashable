﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Cashable.Domain.Entities;

namespace Cashable.DataBase.Repositories
{
    public abstract class BaseRepository<TEntity> where TEntity : class
    {
        protected CashableDbContext mDbContext;
        protected abstract DbSet<TEntity> DbSet { get; }

        public BaseRepository(CashableDbContext dbContext)
        {
            mDbContext = dbContext;
        }

        public IEnumerable<TEntity> Get()
        {
            return DbSet.ToList<TEntity>();
        }

    }
}
