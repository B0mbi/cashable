﻿using System;
using System.Collections.Generic;
using System.Text;
using Cashable.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cashable.DataBase
{
    public class CashableDbContext : DbContext
    {
        public DbSet<TransactionEntity> Transactions { get; set; }

        public CashableDbContext(DbContextOptions options) : base(options)
        {

        }
    }
}
