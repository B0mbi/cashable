﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cashable.Domain.Entities
{
    public class TransactionEntity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public string Date { get; set; }
        public eTransactionType TransactionType { get; set; }
        [NotMapped]
        public bool hasShoppingList { get; }
        public DateTime CreatedDate = DateTime.Now;
    }

    public enum eTransactionType
    {
        None = 0,
        Withdraw = 1,
        Deposit = 2
    }
}