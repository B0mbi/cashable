﻿using System.Collections.Generic;
using Cashable.Domain.Entities;

namespace Cashable.Domain.Repositories
{
    public interface ITransactionRepository
    {
        IEnumerable<TransactionEntity> Get();
        TransactionEntity Get(int id);

        bool Add(TransactionEntity transactionEntity);
        bool Update(int id, TransactionEntity transactionEntity);

        bool Delete(int id);
        void SaveChanges();
    }
}