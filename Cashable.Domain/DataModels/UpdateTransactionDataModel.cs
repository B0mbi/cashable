﻿using System;
using System.Collections.Generic;
using System.Text;
using Cashable.Domain.Entities;

namespace Cashable.Domain.DataModels
{
    public class UpdateTransactionDataModel : ITransaction
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
        public string Date { get; set; }

        public static TransactionEntity operator +(TransactionEntity dest, UpdateTransactionDataModel src)
        {
            dest.Name = src.Name;
            dest.Value = src.Value;
            dest.Date = src.Date;
            return dest;
        }
    }
}
