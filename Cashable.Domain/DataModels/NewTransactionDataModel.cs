﻿using System;
using System.Collections.Generic;
using System.Text;
using Cashable.Domain;
using Cashable.Domain.Entities;

namespace Cashable.Domain.DataModels
{
    public class NewTransactionDataModel : ITransaction
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
        public string Date { get; set; }
        public eTransactionType TransactionType { get; set; }
    }
}
